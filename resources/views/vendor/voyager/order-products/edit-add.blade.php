@php
$edit = !is_null($dataTypeContent->getKey());
$add = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.' . ($edit ? 'edit' : 'add')) . ' ' .
    $dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.' . ($edit ? 'edit' : 'add')) . ' ' . $dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add"
                        action="{{ $edit ? route('voyager.' . $dataType->slug . '.update', $dataTypeContent->getKey()) : route('voyager.' . $dataType->slug . '.store') }}"
                        method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if ($edit)
                            {{ method_field('PUT') }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{$edit ? 'editRows' : 'addRows'};
                            @endphp
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">User</label>
                                <select class="livesearch form-control input-sm" name="user_id">
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">Email</label>
                                <input type="text" class="form-control" id="email" disabled value="">
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">Mobile Number</label>
                                <input type="text" class="form-control" id="contacts" disabled value="">
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">Mobile Number Emg</label>
                                <input type="text" class="form-control" id="contacts_emg" disabled value="">
                            </div>
                            <table class="table table-bordered">
                                <tbody class="thead-dark products">
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Count</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr id="productList">
                                        <td>
                                            <select name="title_product[]" class="form-control title">
                                                <option value="">Select product</option>
                                                <?php $products = DB::table('products')
                                                    ->where('stock', '>', 0)
                                                    ->where('private', 1)
                                                    ->Orwhere('private', 0)
                                                    ->get(); ?>
                                                @foreach ($products as $item)
                                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="number" readonly="" class="form-control price_product"
                                                name="price_product[]"></td>
                                        <td><input type="number" data-field="count_product"
                                                class="form-control count_product" name="count_product[]"></td>
                                        <td><input type="number" data-field="total_product"
                                                class="form-control total_product" value="0" name="total_product[]"></td>
                                        <td><a class="add_button btn btn-primary mb-2">More +</a>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                        @section('submit-buttons')
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        @stop
                        @yield('submit-buttons')
                    </div>
                </form>

                <iframe id="form_target" name="form_target" style="display:none"></iframe>
                <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                    enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                    <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                    <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                    {{ csrf_field() }}
                </form>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                </h4>
            </div>

            <div class="modal-body">
                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                <button type="button" class="btn btn-danger"
                    id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- End Delete File Modal -->
@stop

@section('javascript')
<script>
    var params = {};
    var $file;

    function deleteHandler(tag, isMulti) {
        return function() {
            $file = $(this).siblings(tag);

            params = {
                slug: '{{ $dataType->slug }}',
                filename: $file.data('file-name'),
                id: $file.data('id'),
                field: $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
        };
    }

    $('document').ready(function() {
        $('.toggleswitch').bootstrapToggle();


        $('.livesearch').select2({
            placeholder: 'Select User',
            ajax: {
                url: '/ajax-autocomplete-users',
                dataType: 'json',
                delay: 250,
                processResults: function(data) {

                    console.log(data);

                    return {
                        results: $.map(data, function(item) {
                            console.log(data);
                            data = [];

                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $('select[name="user_id"]').on('change', function() {
            console.log("this.value" + this.value);
            $.ajax({
                url: '/api/v1/users/' + this.value,
                type: "Get",
                dataType: 'json',
                success: function(response) {
                    console.log(response);
                    $("#name").val(response.data.name);
                    $("#email").val(response.data.email);
                    $("#contacts").val(response.data.contacts);
                    $("#contacts_emg").val(response.data.contacts_emg);
                }
            });
        });

        $('body').on('click', '.add_button', function(e) {
            e.preventDefault();
            var clone = $(this).closest('tr').clone(true);
            $("td:last-child", clone).html(
                '<a href="javascript:void(0);" class="delete-row btn btn-danger">Remove</a>'
            );
            clone.insertAfter($(this).closest('tr'));
        });

        $('body').on('click', '.delete-row', function(e) {
            $(this).parent().parent().remove();
        });

        $(document).on("change", ".title", function() {
            var row = $(this).closest('tr');
            $.ajax({
                url: '/api/v1/product/' + this.value,
                type: "Get",
                dataType: 'json',
                success: function(response) {
                    row.find(".price_product").val(response.data['price']);
                    var total = 1 * row.find(".price_product").val()
                    row.find(".total_product").val(total);
                }
            });

        });

        $(document).on("change paste keyup", ".count_product", function() {
            var row = $(this).closest('tr');
            var total = this.value * row.find(".price_product").val()
            console.log(total);
            row.find(".total_product").val(total);
        });

        //Init datepicker for date fields if data-datepicker attribute defined
        //or if browser does not handle date inputs
        $('.form-group input[type=date]').each(function(idx, elt) {
            if (elt.hasAttribute('data-datepicker')) {
                elt.type = 'text';
                $(elt).datetimepicker($(elt).data('datepicker'));
            } else if (elt.type != 'date') {
                elt.type = 'text';
                $(elt).datetimepicker({
                    format: 'L',
                    extraFormats: ['YYYY-MM-DD']
                }).datetimepicker($(elt).data('datepicker'));
            }
        });

        @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
        @endif

        $('.side-body input[data-slug-origin]').each(function(i, el) {
            $(el).slugify();
        });

        $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
        $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
        $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
        $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

        $('#confirm_delete').on('click', function() {
            $.post('{{ route('voyager.' . $dataType->slug . '.media.remove') }}', params, function(
                response) {
                if (response &&
                    response.data &&
                    response.data.status &&
                    response.data.status == 200) {

                    toastr.success(response.data.message);
                    $file.parent().fadeOut(300, function() {
                        $(this).remove();
                    })
                } else {
                    toastr.error("Error removing file.");
                }
            });

            $('#confirm_delete_modal').modal('hide');
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop
