@php
$edit = !is_null($dataTypeContent->getKey());
$add = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .table a {
            text-decoration: auto !important;
        }

        select.hour,
        select.minute {
            color: #76838f;
            background-color: #fff;
            background-image: none;
            border: 1px solid #e4eaec;
        }

        span.combodate {
            display: flex;
        }

    </style>
@stop
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

@section('page_title', __('voyager::generic.' . ($edit ? 'edit' : 'add')) . ' ' .
    $dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.' . ($edit ? 'edit' : 'add')) . ' ' . $dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add" id="formdata"
                        action="{{ $edit ? route('voyager.' . $dataType->slug . '.update', $dataTypeContent->getKey()) : route('voyager.' . $dataType->slug . '.store') }}"
                        method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{$edit ? 'editRows' : 'addRows'};
                            @endphp

                            @foreach ($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? null;
                                    if ($dataTypeContent->{$row->field . '_' . ($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field . '_' . ($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                        style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">
                                        {{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if ($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }}
                                                                                                                                                                                                                    {{ $errors->has($row->field) ? 'has-error' : '' }}"
                                    @if (isset($display_options->id))
                                    {{ "id=$display_options->id" }}
                            @endif>
                            {{ $row->slugify }}
                            <label class="control-label"
                                for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                            @if (isset($row->details->view))
                                @include($row->details->view, ['row' => $row, 'dataType' => $dataType,
                                'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field},
                                'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options'
                                => $row->details])
                            @elseif ($row->type == 'relationship')
                                @include('voyager::formfields.relationship', ['options' => $row->details])
                            @else
                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                            @endif

                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                            @endforeach
                            @if ($errors->has($row->field))
                                @foreach ($errors->get($row->field) as $error)
                                    <span class="help-block">{{ $error }}</span>
                                @endforeach
                            @endif

                        </div>
                        @if ($row->getTranslatedAttribute('display_name') == 'Client')
                            {{-- <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" disabled value="">
                                </div> --}}
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">Email</label>
                                <input type="text" class="form-control" id="email" disabled value="">
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">Mobile Number</label>
                                <input type="text" class="form-control" id="contacts" disabled value="">
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label" for="name">Mobile Number Emg</label>
                                <input type="text" class="form-control" id="contacts_emg" disabled value="">
                            </div>
                        @endif
                        @endforeach

                        <div class="form-group col-md-12" id="bookList">
                            <div id="bookList1">
                                @if ($edit)
                                    <input type="hidden" name="sports" value="{{ $dataTypeContent->sports }}">
                                    <div class="input_fields_wrap">
                                        <table class="table table-bordered" style="margin-bottom: 0;">
                                            <tbody class="thead-dark">
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Time From</th>
                                                    <th>Time To</th>
                                                    <th>Total hours</th>
                                                    <th>Court</th>
                                                    <th>Court size</th>
                                                    <th>Price</th>
                                                    <th>Discount</th>
                                                    <th>Total</th>
                                                    <th></th>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <?php $court_book = DB::table('bookings')
                                                    ->where('id', $dataTypeContent->getKey())
                                                    ->get(); ?>
                                                @foreach ($court_book as $book)
                                                    <tr>
                                                        <td><input type="date" data-field="date"
                                                                value="{{ old('date', date('Y-m-d', strtotime($book->date))) }}"
                                                                class="form-control date" name="date"></td>
                                                        <td><input type="time" step="6000" class="form-control from"
                                                                data-format="HH:mm" data-template="HH:mm" name="from"
                                                                data-field="from" value="{{ $book->from }}" required>
                                                        </td>
                                                        <td><input type="time" step="6000" class="form-control to"
                                                                data-format="HH:mm" data-template="HH : mm" name="to"
                                                                data-field="to" value="{{ $book->to }}" required></td>
                                                        <?php $hourdiff = round((strtotime($book->to) - strtotime($book->from)) / 3600, 1); ?>
                                                        <td><input type="text" data-field="hours" class="form-control hours"
                                                                disabled value="{{ $hourdiff }}" name="date" /></td>
                                                        <td>
                                                            <select name="pitch" data-field="pitch"
                                                                class="form-control pitch">
                                                                <option value="">Select Court</option>
                                                                <?php $tiems = DB::table('courts')->get(); ?>
                                                                @foreach ($tiems as $item)
                                                                    <option value="{{ $item->id }}"
                                                                        @if ($book->pitch == $item->id) selected @endif>{{ $item->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="text" data-field="size"
                                                                value="{{ $book->size }}" class="form-control size"
                                                                readonly name="size"></td>
                                                        <td><input type="number" data-field="price"
                                                                value="{{ $book->price }}" class="form-control price"
                                                                readonly name="price"></td>
                                                        <td><input type="number" data-field="dicsount"
                                                                value="{{ $book->dicsount }}"
                                                                class="form-control discount" value="0" name="discount">
                                                        </td>
                                                        <?php
                                                        $hourdiff = round((strtotime($book->to) - strtotime($book->from)) / 3600, 1);
                                                        $total = $dataTypeContent->price * $hourdiff;
                                                        ?>
                                                        <td><input type="number" data-field="total" readonly
                                                                value="{{ $total }}" class="form-control total"
                                                                name="total"></td>
                                                    </tr>
                                                @endforeach
                                            </tfoot>
                                        </table>
                                    </div>
                                @else
                                    <div class="input_fields_wrap">
                                        <table class="table table-bordered" style="margin-bottom: 0;">
                                            <tbody class="thead-dark">
                                                <tr>
                                                    <th>Sport</th>
                                                    <th>Date</th>
                                                    <th class="time1">Time</th>
                                                    <th class="hours2">Duration</th>
                                                    <th>Court</th>
                                                    <th>Court size</th>
                                                    <th>Price</th>
                                                    <th>Discount</th>
                                                    <th>Total</th>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td>
                                                        <select name="data[0][sport]" data-field="sport"
                                                            class="form-control sport" required>
                                                            <option value="">Select Sport</option>
                                                            <?php $tiems = DB::table('sports')->get(); ?>
                                                            @foreach ($tiems as $item)
                                                                @if (old('sport') == $item->id)
                                                                    <option value="{{ $item->id }}" selected>
                                                                        {{ $item->name }}</option>
                                                                @else
                                                                    <option value="{{ $item->id }}">
                                                                        {{ $item->name }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </td>

                                                    <td><input type="date" min="<?php echo date('Y-m-d'); ?>"
                                                            class="form-control date" name="data[0][date]" data-field="date"
                                                            value="{{ old('date') }}" required></td>
                                                    <td class="time1"><input type="time" step="6000"
                                                            class="form-control from" data-format="HH:mm"
                                                            data-template="HH:mm" name="data[0][from]" data-field="from"
                                                            required>
                                                    </td>
  
                                                    <td class="hours2">
                                                        <select name="data[0][hours]" class="form-control hours" data-field="hours" required>
                                                            <option value="">Select Duration</option>
                                                            <option value="60">60 minutes</option>
                                                            <option value="90">90 minutes</option>
                                                            <option value="120">120 minutes</option>

                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="data[0][pitch]" data-field="pitch"
                                                            class="form-control pitch" required>
                                                            <option value="">Select Court</option>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" class="form-control size" readonly
                                                            data-field="size" name="data[0][size]"
                                                            value="{{ old('size') }}"></td>
                                                    <td><input type="number" class="form-control price" readonly
                                                            name="data[0][price]" data-field="price"
                                                            value="{{ old('price') }}"></td>
                                                    <td><input type="number" class="form-control discount"
                                                            name="data[0][discount]" data-field="discount"
                                                            value="{{ old('discount') }}" required></td>
                                                    <td><input type="number" class="form-control total" readonly
                                                            name="data[0][total]" data-field="total"
                                                            value="{{ old('total') }}"></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                @endif
                                @if ($edit)
                                    <input type="hidden" name="id" value="{{ $dataTypeContent->getKey() }}">
                                    <div class="input_fields_wrap">
                                        <table class="table table-bordered">
                                            <tbody class="thead-dark products">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Price</th>
                                                    <th>Count</th>
                                                    <th>Total</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <?php $OrderProduct = App\OrderProduct::where('book_id', $dataTypeContent->getKey())->get(); ?>
                                                @foreach ($OrderProduct as $items)
                                                    <tr>
                                                        <td>
                                                            <select name="data[0][title_product][]"
                                                                class="form-control title_product">
                                                                <?php $products = DB::table('products')
                                                                    ->where('stock', '>', 0)
                                                                    ->where('private', 1)
                                                                    ->Orwhere('private', 0)
                                                                    ->get(); ?>
                                                                @foreach ($products as $item)
                                                                    <option value="{{ $item->id }}"
                                                                        @if ($items->title == $item->id) selected @endif>
                                                                        {{ $item->title }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="number" class="form-control price_product" readonly
                                                                data-field="price_product" value="{{ $items->price }}"
                                                                readonly name="data[0][price_product][]"></td>
                                                        <td><input type="number" class="form-control count_product"
                                                                data-field="count_product" value="{{ $items->count }}"
                                                                name="data[0][count_product][]"></td>
                                                        <td><input type="number" class="form-control total_product"
                                                                data-field="total_product" value="{{ $items->total }}"
                                                                value="0" readonly name="data[0][total_product][]"></td>
                                                        <td><a href="{{ url('order/product', $items->id) }}"
                                                                class="delete-row btn btn-danger mb-2">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                @if ($OrderProduct->isEmpty())
                                                    <tr id="productList">
                                                        <td>
                                                            <select data-type="1" data-field="title_product"
                                                                name="data[0][title_product][]" class="form-control title">
                                                                <option value="">Select product</option>
                                                                <?php $products = DB::table('products')
                                                                    ->where('stock', '>', 0)
                                                                    ->get(); ?>
                                                                @foreach ($products as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{ $item->title }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="number" readonly data-type="1"
                                                                data-field="price_product"
                                                                class="form-control price_product"
                                                                name="data[0][price_product][]"></td>
                                                        <td><input type="number" data-type="1" data-field="count_product"
                                                                class="form-control count_product"
                                                                name="data[0][count_product][]"></td>
                                                        <td><input type="number" data-type="1" data-field="total_product"
                                                                class="form-control total_product" value="0"
                                                                name="data[0][total_product][]"></td>
                                                        <td><a class="add_button btn btn-primary mb-2">More +</a>
                                                        </td>
                                                    </tr>

                                                @endif
                                            </tfoot>
                                        </table>
                                    </div>
                                @else
                                    <div class="input_fields_wrap">
                                        <table class="table table-bordered">
                                            <tbody class="thead-dark products">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Price</th>
                                                    <th>Count</th>
                                                    <th>Total</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr id="productList">
                                                    <td>
                                                        <select data-type="1" data-field="title_product"
                                                            name="data[0][title_product][]" class="form-control title">
                                                            <option value="">Select product</option>
                                                            <?php $products = DB::table('products')
                                                                ->where('stock', '>', 0)
                                                                ->get(); ?>
                                                            @foreach ($products as $item)
                                                                <option value="{{ $item->id }}">{{ $item->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input type="number" readonly data-type="1"
                                                            data-field="price_product" class="form-control price_product"
                                                            name="data[0][price_product][]"></td>
                                                    <td><input type="number" data-type="1" data-field="count_product"
                                                            class="form-control count_product"
                                                            name="data[0][count_product][]"></td>
                                                    <td><input type="number" data-type="1" data-field="total_product"
                                                            class="form-control total_product" value="0"
                                                            name="data[0][total_product][]"></td>
                                                    <td><a class="add_button btn btn-primary mb-2">More +</a>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                            </div>
                            <button class="add_field_book btn btn-primary mb-2">Add New Book</button>

                            @endif
                        </div>

                        <div class="form-group  col-md-12 ">

                        </div>

                        <div class="form-group  col-md-12 ">
                            <label class="control-label">Total Amount </label>
                            <table class="table table-bordered">
                                @if ($edit)
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 10%;">Rental Fees</th>
                                            <th scope="col">AED <span id="renatl">{{ $dataTypeContent->total }}</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 10%;">Add-ons</th>
                                            <th scope="col">AED <span
                                                    id="add_ons">{{ $OrderProduct->sum('total') }}</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 10%;">Total</th>
                                            <th scope="col">AED <span id="total_all">{{ $dataTypeContent->total }}</span>
                                            </th>
                                        </tr>
                                    </thead>
                                @else
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 10%;">Rental Fees</th>
                                            <th scope="col">AED <span id="renatl">00.00</span></th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 10%;">Add-ons</th>
                                            <th scope="col">AED <span id="add_ons">00.00</span></th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 10%;">Total</th>
                                            <th scope="col">AED <span id="total_all">00.00</span></th>
                                        </tr>
                                    </thead>
                                @endif
                            </table>
                        </div>
                </div><!-- panel-body -->

                <div class="errorTxt" id="newsletterValidate"></div>
                <div class="panel-footer">
                @section('submit-buttons')
                    <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                @stop
                @yield('submit-buttons')
            </div>
            </form>

            <iframe id="form_target" name="form_target" style="display:none"></iframe>
            <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                {{ csrf_field() }}
            </form>

        </div>
    </div>
</div>
</div>


<div class="modal fade modal-danger" id="confirm_delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                </h4>
            </div>

            <div class="modal-body">
                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                <button type="button" class="btn btn-danger"
                    id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- End Delete File Modal -->
@stop

@section('javascript')
<script src="https://vitalets.github.io/combodate/momentjs/moment.min.2.5.0.js"></script>
<script src="https://vitalets.github.io/combodate/combodate.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script>
    var params = {};
    var $file;

    function deleteHandler(tag, isMulti) {
        return function() {
            $file = $(this).siblings(tag);

            params = {
                slug: '{{ $dataType->slug }}',
                filename: $file.data('file-name'),
                id: $file.data('id'),
                field: $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
        };
    }



    $('document').ready(function() {
        $('.toggleswitch').bootstrapToggle();

        function CalculateTotal() {
            var AllTotal = 0;
            $('.total').each(function() {
                AllTotal += parseFloat(this.value);
            });

            var AllTotalProduct = 0;
            $('.total_product').each(function() {
                AllTotalProduct += parseFloat(this.value);
            });

            // var total = document.getElementById("renatl").innerHTML;
            // var add_ons = document.getElementById("add_ons").innerHTML;
            var sum = +Number(AllTotalProduct) + Number(AllTotal);
            console.log(sum);
            $("#total_all").html('');
            $("#renatl").html('');
            $("#add_ons").html('');

            $("#add_ons").append(AllTotalProduct);
            $("#renatl").append(AllTotal);
            $("#total_all").append(sum);

        }

        $(document).ready(function() {
            var day;
            $(".from").combodate({
                minuteStep: 15
            });
            @if ($edit)
                var splashArray = $('[name="sports"]').val()
            @else
                var splashArray = [];
            @endif
            var i = 0;
            $('body').on('click', '.add_field_book', function(e) {
                e.preventDefault();
                var $div = $('div[id^="bookList"]:last');
                var num = parseInt($div.prop("id").match(/\d+/g), 10) + 1;
                var $klon = $div.clone(false, false).prop('id', 'bookList' + num);
                $klon.find('.add_field_book').remove();
                $div.after($klon);
                i++;
                $klon.find('input').each(function() {
                    const fieldname = $(this).attr('data-field');
                    const dataType = $(this).attr('data-type');
                    if (dataType == 1) {
                        $(this).attr('name', 'data[' + i + '][' + fieldname + '][]');
                    } else {
                        $(this).attr('name', 'data[' + i + '][' + fieldname + ']');
                    }
                });
                $klon.find('select').each(function() {
                    const fieldname = $(this).attr('data-field');
                    const dataType = $(this).attr('data-type');
                    if (dataType == 1) {
                        $(this).attr('name', 'data[' + i + '][' + fieldname + '][]');
                    } else {
                        $(this).attr('name', 'data[' + i + '][' + fieldname + ']');
                    }
                });
                $klon.find(".count_product").val('');
                $klon.find(".total_product").val(0);
                $klon.find(".price_product").val('');
                $klon.find(".date").val('');
                $klon.find(".from").val('');
                $klon.find(".to").val('');
                $klon.find(".hours").val('');
                $klon.find(".size").val('');
                $klon.find(".price").val('');
                $klon.find(".total").val('');
                $klon.find(".combodate").remove();
                $(".from").combodate({
                    minuteStep: 15
                });
            });

            $(".from").combodate({
                minuteStep: 15
            });

            $('body').on('click', '.add_button', function(e) {
                e.preventDefault();
                var clone = $(this).closest('tr').clone(true);
                $("td:last-child", clone).html(
                    '<a href="javascript:void(0);" class="delete-row btn btn-danger">Remove</a>'
                );
                clone.insertAfter($(this).closest('tr'));
            });

            $('body').on('click', '.delete-row', function(e) {
                $(this).parent().parent().remove();
                var sum = 0;
                $(".total_product").each(function() {
                    sum += +$(this).val();
                });
                console.log(sum);
                CalculateTotal();
                $("#add_ons").html('');
                $("#add_ons").append(sum);
            });

            $("table").on("click", ".delete-row", function(event) {
                $(this).closest("tr").remove();
                var sum = 0;
                $(".total_product").each(function() {
                    sum += +$(this).val();
                });
                console.log(sum);
                CalculateTotal();
                $("#add_ons").html('');
                $("#add_ons").append(sum);
            });

            $(".delete_time").click(function(e) {
                e.preventDefault();
                var id = $(this).attr('id');
                $('#item' + id).remove();
                $.ajax({
                    url: "/delete/" + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res) {
                        console.log(res);
                    }
                });
            });

            // function createOption(value, text) {
            //     var option = document.createElement('option');
            //     option.text = text;
            //     option.value = value;
            //     return option;
            // }

            // var hourSelect = document.getElementById('hours hours_to');
            // console.log(hourSelect);
            // for (var i = 0; i <= 23; i++) {
            //     hourSelect.add(createOption(i, i));
            // }

            // var minutesSelect = document.getElementsByClassName('minutes');
            // for (var i = 0; i < 60; i += 15) {
            //     minutesSelect.add(createOption(i, i));
            // }

            $('.save').on('click', function(e) {
                e.preventDefault();
                var client_id = $(".select2-ajax").val();
                var status = $("select[name=status]").val();
                var penalty = $("input[name=penalty]").val();
                var from = $('.from').val()
                var to = $('.to').val()
                var date = $('.date').val()
                var sport = $(".sport").val();
                var pitch = $('.pitch').val()
                var size = $("input[name=size]").val();
                var price = $("input[name=price]").val();
                var discount = $("input[name=discount]").val();
                var total = $("input[name=total]").val();
                if (client_id == "") {
                    toastr.error("Client selection is required to confirm bookingd");
                    return;
                }
                if (penalty == "") {
                    toastr.error("Penalty selection is required to confirm bookingd");
                    return;
                }
                if (date == "") {
                    toastr.error("Date selection is required to confirm bookingd");
                    return;
                }
                if (from == "") {
                    toastr.error("Time selection is required to confirm booking");
                    return;
                }
                if (to == "") {
                    toastr.error("Time selection is required to confirm bookingd");
                    return;
                }
                if (sport == "") {
                    toastr.error("Sport selection is required to confirm booking");
                    return;
                }
                if (pitch == "") {
                    toastr.error("Pitch/Court selection is required to confirm booking");
                    return;
                }
                var values = $('.pitch', '.from', '.to', '.date').map(function() {
                    console.log(1);
                    return this.value;
                }).toArray();

                var hasDups = !values.every(function(v, i) {
                    console.log(v);
                    return values.indexOf(v) == i;
                });
                if (hasDups) {
                    console.log("please do not repeat the pitch");
                    toastr.error("please do not repeat the pitch");
                    e.preventDefault();
                }
                $(this).attr('disabled','disabled');
                $(this).html('<span class="spinner"><i class="fa fa-spinner fa-spin"></i></span>Loading.....');
                $.ajax({
                    url: '{{ $edit ? '/edit/book' : '/check/book' }}',
                    type: "POST",
                    data: $("#formdata").serialize() + '&' + 'sports=' + splashArray,
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        console.log(response.state);
                        if (response.state == 200) {
                            window.location.href = "{{ url('admin/bookings') }}";
                            toastr.success(response.msg);
                        } else {
                            $(':input[type="submit"]').prop('enable', true);
                            toastr.error(response.msg);
                        }
                    }
                });
            });

            $('select[name="client_id"]').on('change', function() {
                console.log("this.value" + this.value);
                $.ajax({
                    url: '/api/v1/users/' + this.value,
                    type: "Get",
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        $("#name").val(response.data.name);
                        $("#email").val(response.data.email);
                        $("#contacts").val(response.data.contacts);
                        $("#contacts_emg").val(response.data.contacts_emg);
                    }
                });
            });

            $(document).on("change", '.title', function() {
                var row = $(this).closest('tr');
                console.log("value" + this.value);
                $.ajax({
                    url: '/check/products/' + this.value,
                    type: "Get",
                    dataType: 'json',
                    success: function(response) {
                        row.find('.price_product').val(response.data.price);
                        row.find('.count_product').val(1);
                        row.find('.total_product').val(response.data.price);
                        CalculateTotal()

                    }
                });
            });

            $(document).on("change paste keyup", '.count_product', function() {
                var row = $(this).closest('tr');
                console.log(row);
                var price = row.find('.price_product').val();
                var total = this.value * price
                row.find('.total_product').val(total);
                var sum = 0;
                row.find(".total_product").each(function() {
                    sum += +$(this).val();
                });
                console.log(sum);
                $("#add_ons").html('');
                $("#add_ons").append(sum);
                CalculateTotal()

            });

            $(document).on("change", '.pitch', function() {
                console.log("pitch" + this.value);
                var row = $(this).closest('tr');
                splashArray = [];
                $.ajax({
                    url: '/api/v1/courts/' + this.value,
                    type: "Get",
                    dataType: 'json',
                    success: function(response) {
                        console.log(response.data);
                        console.log(response.data.rate);
                        if(response.data.sports == 5){
                            console.log(response.data.rate);
                            row.find('.total').val(response.data.rate);
                            row.find('.price').val(response.data.rate);
                        }else{
                            var hour = row.find('.hours').val();
                            var all = response.data.rate * hour /60
                            row.find('.price').val(all);
                            row.find('.total').val(all);
                            row.find('.discount').val(0);
                            row.find(".size").val(response.data.size);
                        }
                        response.data.sport.forEach(function(item) {
                            splashArray.push(item['sport_id']);
                        });
                        CalculateTotal();

                    }
                });

            });

            $(document).on("change", '.hours', function() {
                console.log(this.value);
                var row = $(this).closest('tr');
                var sport = row.find('.sport').val()
                var from = row.find('.from').val()
                var to = row.find('.to').val()
                var pitch = row.find('.pitch').val()
                var date = row.find('.date').val()
                console.log(from);

                if (row.find('.pitch').val()) {
                    var hour = row.find('.hours').val();
                    var price = row.find('.price').val()
                    var all = price * hour / 60
                    row.find('.total').val(all);
                    $("#renatl").html('');
                    $("#total_all").html('');
                    $("#renatl").append(all);
                    $("#total_all").append(all);
                } else {
                    $.ajax({
                        url: '/check/sport',
                        type: "POST",
                        data: {
                            sport: sport,
                            day: day,
                            from: row.find('.hour').val() + ':' + row.find(
                                '.minute').val(),
                            duration: row.find('.hours').val(),
                        },
                        dataType: 'json',
                        success: function(response) {
                            row.find('.pitch').html('');
                            var pitchInput = row.find('.pitch')
                            jQuery('<option/>', {
                                value: '',
                                html: 'Select Court'
                            }).appendTo(pitchInput);
                            for (var i = 0; i < response.data.length; i++) {
                                jQuery('<option/>', {
                                    value: response.data[i]['id'],
                                    html: response.data[i]['name']
                                }).appendTo(pitchInput);
                            }
                        }
                    });
                }

            });

            $(document).on("change", '.sport', function() {
                var row = $(this).closest('tr');
                console.log(this.value);
                if (this.value == 5) {
                    var row = $(this).closest('tr');
                    row.find('.time1').hide();
                    row.find('.time2').hide();
                    row.find('.hours2').hide();
                    $.ajax({
                        url: '/check/sport',
                        type: "POST",
                        data: {
                            sport: 5,
                        },
                        dataType: 'json',
                        success: function(response) {
                            row.find('.pitch').html('');
                            var pitchInput = row.find('.pitch')
                            jQuery('<option/>', {
                                value: '',
                                html: 'Select Court'
                            }).appendTo(pitchInput);
                            for (var i = 0; i < response.data.length; i++) {
                                jQuery('<option/>', {
                                    value: response.data[i]['id'],
                                    html: response.data[i]['name']
                                }).appendTo(pitchInput);
                            }
                        }
                    });
                } else {
                    row.find('.time1').show();
                    row.find('.time2').show();
                    row.find('.hours2').show();
                }
            });



            $(document).on("change", '.to', function() {
                var row = $(this).closest('tr');
                console.log("to " + row.find('.to').val());
                var h = getDifference(row.find('.to').val(), row.find('.from').val())
                row.find(".hours").val(h);
                row.find('.total').val(h * row.find('.price').val());
                console.log(h);
                console.log(row.find('.price').val());
            });

            let getDifference = (time1, time2) => {
                let [h1, m1] = time1.split(':')
                let [h2, m2] = time2.split(':')

                return ((+h1 + (+m1 / 60)) - (+h2 + (+m2 / 60)))
            }

            $(document).on("change paste keyup", '.date', function() {
                var id = $(this).attr('id');
                var row = $(this).closest('tr');
                console.log(row);
                day = new Date(row.find(".date").val()).getDay();
                switch (new Date(row.find(".date").val()).getDay()) {
                    case 0:
                        day = "Sunday";
                        break;
                    case 1:
                        day = "Monday";
                        break;
                    case 2:
                        day = "Tuesday";
                        break;
                    case 3:
                        day = "Wednesday";
                        break;
                    case 4:
                        day = "Thursday";
                        break;
                    case 5:
                        day = "Friday";
                        break;
                    case 6:
                        day = "Saturday";
                }
            });


            $(document).on("change paste keyup", '.discount', function() {
                var row = $(this).closest('tr');
                var value = row.find(".price").val() * this.value / 100;
                console.log(value);
                var v = row.find(".price").val() - value
                row.find('.total').val(v);
                $("#renatl").html('');
                $("#renatl").append(v);
                CalculateTotal();
            });


            $('.form-group input[type=date]').each(function(idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function() {
                $.post('{{ route('voyager.' . $dataType->slug . '.media.remove') }}', params,
                    function(
                        response) {
                        if (response &&
                            response.data &&
                            response.data.status &&
                            response.data.status == 200) {

                            toastr.success(response.data.message);
                            $file.parent().fadeOut(300, function() {
                                $(this).remove();
                            })
                        } else {
                            toastr.error("Error removing file.");
                        }
                    });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    });
</script>
@stop
