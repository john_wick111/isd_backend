<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>@yield('title') - ISD</title>
	<meta name="title" content="page_title">
	<meta name="keywords" content="@yield('keywords')" />
	<meta name="description" content="@yield('description')" />
	<link rel="icon" type="image/x-icon" href="{{asset('public/assets-web/images/favicon.ico')}}" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="canonical" href="{{ url()->current() }}" />
	<link rel="stylesheet" type="text/css" href="{{asset('public/assets-web/css/style.css')}}"/>
	<meta name="google-site-verification" content="0KJIv5gJ4UY7MLLDkRYbpMVYtKiL_LW-v89Yaz2pXqg" />
	
	<noscript id="deferred-styles">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/temp-css/style.css"> -->
	</noscript>
	<script>
	var loadDeferredStyles = function() {
	var addStylesNode = document.getElementById("deferred-styles");
	var replacement = document.createElement("div");
	replacement.innerHTML = addStylesNode.textContent;
	document.body.appendChild(replacement)
	addStylesNode.parentElement.removeChild(addStylesNode);
	};
	var raf = requestAnimationFrame || mozRequestAnimationFrame ||
	webkitRequestAnimationFrame || msRequestAnimationFrame;
	if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
	else window.addEventListener('load', loadDeferredStyles);
	</script>
	<style>
	.no-js #loader { display: none;  }
	.js #loader { display: block; position: absolute; left: 100px; top: 0; }
	.bg-init,
	.animsition-overlay-slide {
	display: block;
	background-image: url("{{asset('public/assets-web/images/logo-white-icon.svg')}}");
	background-size: 10%;
	background-position: center;
	background-repeat: no-repeat;
	}
	/*.pre-loading {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url('/assets-web/images/loader.gif') center no-repeat #fff;
	}*/
	</style>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	
	<script type="text/javascript" src="{{ asset('public/assets-web/js/functions.js')}}"></script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '470356704130813');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=470356704130813&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Global site tag (gtag.js) - Google Analytics + Google Ad -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-755D7DKZNG"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'G-755D7DKZNG');
	gtag('config', 'AW-432844860');
	</script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- tracking code -->
	<script type='text/javascript'>
	window.smartlook||(function(d) {
	var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
	})(document);
	smartlook('init', '4e027d71682fae2b7690f0231406963dae359aea');
	</script>
	<!-- End tracking code -->
	<!-- Snap Pixel Code for all pages except thank you-->
	<script type='text/javascript'>
	(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function()
	{a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)};
	a.queue=[];var s='script';r=t.createElement(s);r.async=!0;
	r.src=n;var u=t.getElementsByTagName(s)[0];
	u.parentNode.insertBefore(r,u);})(window,document,
	'https://sc-static.net/scevent.min.js');
	snaptr('init', 'a1f73a38-c876-4c73-8f3e-5aad2d01c9dd', {
	'user_email': '__INSERT_USER_EMAIL__'
	});
	snaptr('track', 'PAGE_VIEW');
	</script>
	<!-- End Snap Pixel Code -->
</head>
<body class="@yield('pageClass') bg-init">
	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--<div class="pre-loading"></div>-->
	
	<main class="app-container animsition-overlay">
		
		
		@yield('content')

	</main>
	<!--    <footer class="primary-footer">
				<div class="container">
							<p class="desc">Copyright &copy; <span id="cur-year"></span> Repo. All Rights Reserved. Site by </p>
				</div>
	</footer> -->
	<!-- <div class="overlay-bg"></div> -->

	<div class="inquire-now hidden show">
		<a href="#" data-targetmodal="x-modal">Inquire Now</a>
	</div>


	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<?php /* <script type="text/javascript" src="http://web.simmons.edu/~grovesd/comm244/demo/skrollr/examples/road-trip-simple/js/skrollr.min.js"></script> */ ?>
	<script type="text/javascript" src="{{ asset('public/assets-web/js/script.js')}}"></script>
	<!-- <script src="/temp-js/main.js"></script> -->
	<script>
	$(window).load(function() {
	// Animate loader off screen
	$(".pre-loading").fadeOut("slow");
	if ($(window).width() >= 1200) {
	var s = skrollr.init();
	}
	// skrollr.init();
	// var _skrollr = skrollr.get(); // get() returns the skrollr instance or undefined
	// var windowWidth = $(window).width();
	// if ( windowWidth <= 640 && _skrollr !== undefined ) {
	//   skrollr.init().destroy();
	// }
	});
	var dateToday = new Date();
	$( "#date_of_requirement" ).datepicker({
	
	});
	var dateBirth = new Date();
	$( "#dob" ).datepicker({
	changeMonth: true,
	changeYear: true,
	maxDate: new Date('2020-12-31')
	});
	$(document).ready(function() {
	$('.animsition-overlay').animsition({
	inClass: 'overlay-slide-in-left',
	outClass: 'overlay-slide-out-left',
	overlay : true,
	overlayClass : 'animsition-overlay-slide',
	overlayParentElement : 'body'
	})
	.one('animsition.inStart',function(){
	$('body').removeClass('bg-init');
	})
	
	.one('animsition.inEnd',function(){
	});
	});
	</script>
	@yield('footer-scripts')
</body>
</html>