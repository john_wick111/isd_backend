@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    {!! Form::open(['route' => 'client.bookings.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Review Amount</h1>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Card Example -->
            <div class="card mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Booking Details</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                            @foreach($details as $key=> $detail)
                            
                            <th>
                                {{$key}}
                            </th>
                            
                            
                            @endforeach
                        </thead>
                        
                        <tbody>
                            <tr>
                                @foreach($details as $key=> $detail)
                                
                                <td>
                                    {{$detail}}<br>
                                </td>
                                
                                
                                @endforeach
                            </tr>
                        </tbody>
                        
                    </table>
                     <button type="submit" class="btn btn-primary">Pay Now</button>
                </div>
            </div>
            
        </div>
    </div>
    
    {!! Form::close() !!}
</div>
<!-- /.container-fluid -->
@endsection