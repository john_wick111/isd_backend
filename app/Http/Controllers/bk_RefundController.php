<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Court_times;
use App\Booking;
use DB;
use App\Exports\BookingExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Subscription;
use App\Academy;
use App\User;
use App\Payment;
use App\pivote_academy_payment;
use App\Wallet;
use Carbon\Carbon as Carbon;

class RefundController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->data;

        $pivote_academy_payment = pivote_academy_payment::where('book_id', $data['book_id'])->where('client_id', $data['client_id'])->first();

        return view('voyager::refund.edit-add', compact('data', 'pivote_academy_payment'));
    }

    public function post(Request $request)
    {

        // dd($request->book_id);

        $user_id = $request->client_id;
        $booking_id = $request->book_id;

        // validation
        $request->validate([

            'refund_amnt' => 'required|numeric',
            'refund_resn' => 'required',

        ], [

            'refund_amnt.required' => 'The Amount must be a number.',
            'refund_resn.required' => 'Reason is required'

        ]);

        $walletChecking = Wallet::where('client_id', $user_id)->doesntExist();

        if ($walletChecking) {

            $get_wallet_id  = Wallet::insertGetId([
                'client_id' =>  $request->client_id,
                'created_at' =>  Carbon::now()->toDateTimeString(),
            ]);

            $wallet = Wallet::find($get_wallet_id);
            $wallet->client_id = $user_id;
            $wallet->credit_amount = $request->refund_amnt;
            $wallet->remarks = $request->refund_resn;
            $wallet->total = $request->refund_amnt;
            $wallet->is_active = STATUS_ACTIVE;
            $wallet->created_at = Carbon::now()->toDateTimeString();
            $wallet->save();


            $pivote_academy_payment = new pivote_academy_payment;
            $pivote_academy_payment->book_id = $request->book_id;
            $pivote_academy_payment->refund_amnt = $request->refund_amnt;
            $pivote_academy_payment->refund_resn = $request->refund_resn;
            $pivote_academy_payment->type = TYPE_REFUND;
            $pivote_academy_payment->client_id = $user_id;
            $pivote_academy_payment->client_wallet_id = $get_wallet_id;
            $pivote_academy_payment->save();

        } else {

            // if refund update with wallet so check if wallet exist
            $ifRefundExist = pivote_academy_payment::where('book_id', $booking_id)->where('client_id', $user_id)->exists();

            if ($ifRefundExist) {

               

                $updateRefund = pivote_academy_payment::where('book_id', $booking_id)->where('client_id', $user_id)->first();

                // dd($updateRefund);

                // update refund amount and reason
                pivote_academy_payment::where('book_id', $booking_id)->where('client_id', $user_id)
                    ->update([
                        'refund_amnt' => $request->refund_amnt,
                        'refund_resn' => $request->refund_resn,
                    ]);

                    // $updateRefund->client_wallet_id;
                    $wallet = Wallet::find($updateRefund->client_wallet_id);
                    $wallet->client_id = $user_id;
                    $wallet->credit_amount = $request->refund_amnt;
                    $wallet->remarks = $request->refund_resn;
                    $wallet->total = $request->refund_amnt;
                    $wallet->is_active = STATUS_ACTIVE;
                    $wallet->created_at = Carbon::now()->toDateTimeString();
                    $wallet->save();




            } else {

                $last_wllt_amnt = Wallet::where('client_id', $user_id)->orderBy('id', 'DESC')->first();

                // dd($last_wllt_amnt->total);

                $final_amnt = $last_wllt_amnt->total + $request->refund_amnt;
               
                $get_wallet_id  = Wallet::insertGetId([
                    'client_id' =>  $request->client_id,
                    'credit_amount' =>  $request->refund_amnt,
                    'remarks' =>  $request->refund_resn,
                    'total' =>  $final_amnt,
                    'is_active' =>  STATUS_ACTIVE,
                    'created_at' =>  Carbon::now()->toDateTimeString(),
                ]);

                $pivote_academy_payment = new pivote_academy_payment;
                $pivote_academy_payment->book_id = $request->book_id;
                $pivote_academy_payment->refund_amnt = $request->refund_amnt;
                $pivote_academy_payment->refund_resn = $request->refund_resn;
                $pivote_academy_payment->type = TYPE_REFUND;
                $pivote_academy_payment->client_id = $user_id;
                $pivote_academy_payment->client_wallet_id = $get_wallet_id;
                $pivote_academy_payment->save();
            }
        }




        return back()->with('success', 'Booking Updated Successfully!');
    }
}
