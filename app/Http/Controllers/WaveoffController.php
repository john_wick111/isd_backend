<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Court_times;
use App\Booking;
use DB;
use App\Exports\BookingExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Subscription;
use App\Academy;
use App\User;
use App\Payment;
use App\pivote_academy_payment;
use App\Wallet;
use Carbon\Carbon as Carbon;

class WaveoffController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->data;

        $pivote_academy_payment = pivote_academy_payment::where('book_id', $data['book_id'])->where('client_id', $data['client_id'])->first();

        return view('voyager::waveoff.edit-add', compact('data', 'pivote_academy_payment'));
    }

    public function post(Request $request)
    {

        // dd($request->all());

        $user_id = $request->client_id;
        $booking_id = $request->book_id;

        // validation
        $request->validate([

            'waveoff_amnt' => 'required|numeric',
            'waveoff_resn' => 'required',

        ], [

            'waveoff_amnt.required' => 'The Amount must be a number.',
            'waveoff_resn.required' => 'Reason is required'

        ]);

        // $walletChecking = Wallet::where('client_id', $user_id)->doesntExist();

        // if ($walletChecking) {

        //     $get_wallet_id  = Wallet::insertGetId([
        //         'client_id' =>  $request->client_id,
        //         'created_at' =>  Carbon::now()->toDateTimeString(),
        //     ]);

            // $wallet = Wallet::find($get_wallet_id);
            // $wallet->client_id = $user_id;
            // $wallet->credit_amount = $request->waveoff_amnt;
            // $wallet->remarks = $request->waveoff_resn;
            // $wallet->total = $request->waveoff_amnt;
            // $wallet->is_active = STATUS_ACTIVE;
            // $wallet->created_at = Carbon::now()->toDateTimeString();
            // $wallet->save();


            $pivote_academy_payment = new pivote_academy_payment;
            $pivote_academy_payment->book_id = $request->book_id;
            $pivote_academy_payment->waveoff_amnt = $request->waveoff_amnt;
            $pivote_academy_payment->waveoff_resn = $request->waveoff_resn;
            $pivote_academy_payment->type = TYPE_WAVEOFF;
            $pivote_academy_payment->client_id = $user_id;
            // $pivote_academy_payment->client_wallet_id = $get_wallet_id;
            $pivote_academy_payment->save();

        // } else {

            // // if refund update with wallet so check if wallet exist
            // $ifRefundExist = pivote_academy_payment::where('book_id', $booking_id)->where('client_id', $user_id)->exists();

            // if ($ifRefundExist) {

               

            //     $updateRefund = pivote_academy_payment::where('book_id', $booking_id)->where('client_id', $user_id)->first();

            //     // dd($updateRefund);

            //     // update refund amount and reason
            //     pivote_academy_payment::where('book_id', $booking_id)->where('client_id', $user_id)
            //         ->update([
            //             'waveoff_amnt' => $request->waveoff_amnt,
            //             'waveoff_resn' => $request->waveoff_resn,
            //         ]);

            //         // $updateRefund->client_wallet_id;
            //         $wallet = Wallet::find($updateRefund->client_wallet_id);
            //         $wallet->client_id = $user_id;
            //         $wallet->credit_amount = $request->waveoff_amnt;
            //         $wallet->remarks = $request->waveoff_resn;
            //         $wallet->total = $request->waveoff_amnt;
            //         $wallet->is_active = STATUS_ACTIVE;
            //         $wallet->created_at = Carbon::now()->toDateTimeString();
            //         $wallet->save();




            // } else {

            //     $last_wllt_amnt = Wallet::where('client_id', $user_id)->orderBy('id', 'DESC')->first();

            //     // dd($last_wllt_amnt->total);

            //     $final_amnt = $last_wllt_amnt->total + $request->refund_amnt;
               
            //     $get_wallet_id  = Wallet::insertGetId([
            //         'client_id' =>  $request->client_id,
            //         'credit_amount' =>  $request->waveoff_amnt,
            //         'remarks' =>  $request->waveoff_resn,
            //         'total' =>  $final_amnt,
            //         'is_active' =>  STATUS_ACTIVE,
            //         'created_at' =>  Carbon::now()->toDateTimeString(),
            //     ]);

            //     $pivote_academy_payment = new pivote_academy_payment;
            //     $pivote_academy_payment->book_id = $request->book_id;
            //     $pivote_academy_payment->waveoff_amnt = $request->waveoff_amnt;
            //     $pivote_academy_payment->waveoff_resn = $request->waveoff_resn;
            //     $pivote_academy_payment->type = TYPE_WAVEOFF;
            //     $pivote_academy_payment->client_id = $user_id;
            //     $pivote_academy_payment->client_wallet_id = $get_wallet_id;
            //     $pivote_academy_payment->save();
            // }
        // }

        return back()->with('success', 'Booking Updated Successfully!');
    }
}
