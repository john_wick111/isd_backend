<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Validator;
use Auth;
use DB;
use Mail;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $check = User::where('email',$request->email)->count();
        if($check > 0){
            return response()->json([
                'status'=>500,
                'msg'=>' This email is already in use ',
            ]);
        }
        
        $check2 = User::where('contacts',$request->contacts)->count();
        if($check2 > 0){
            return response()->json([
                'status'=>500,
                'msg'=>' This contacts is already in use ',
            ]);            
        }

        $User = new User;
        $User->password = Hash::make($request->password);
        $User->email = $request->email;
        $User->name = $request->name;
        $User->contacts = $request->contacts;
        $User->code = $this->generateRandomString(10);
        $User->avatar = "users/default.png";
        $User->parent = $request->parent ? $request->parent : 0 ;
        $User->role_id = 2;
        $activation_code = $this->generateRandomString(5);
        $User->activation_code =$activation_code;
        $User->save();
        $User->access_token = $User->createToken('TutsForWeb')->accessToken;

        $details = [
            'email' => $request->email,
            'activation_code' => $activation_code,
            'id' => $User->id,
        ];
        Mail::to($request->email)->send(new \App\Mail\SendMailRegistration($details));

        return response()->json([
            'status'=>200,
            'msg'=>' successfully ',
            'data'=>$User
        ]);

    }

    public function Error(Request $request){
        ini_set("SMTP", "smtp.gmail.com");
        ini_set("sendmail_from", "sehsahdeveloper@gmail.com");
        $message = "New Bug ";
        $message .= " { ".$request->msg." } in page (".$request->page.")";
        $headers = "From: info@myisd.com";
        $headers .= "MIME-Version: 1.0" . "\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
        mail("sehsah.error@gmail.com", "Bug In MY ISD", $message, $headers);

        return response()->json([
            'status'=>200,
            'msg'=>' successfully ',
        ]);
    }

    public function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

        public function login(Request $request)
        {
            if (Auth::attempt(['email'=> $request->email,'password'=> $request->password])) {
                $user = Auth::User();
                if($user->status == 1){
                    $user->access_token = $user->createToken('TutsForWeb')->accessToken;
                    return response()->json([
                        'data'=>$user,
                        'status'=>200
                    ]);
                }else{
                    return response()->json([
                        'status'=>505,
                        'data'=> '0',
                        'msg'=>'Your account does not have access to the system'
                    ]);
                }
            }else{
                return response()->json([
                    'status'=>500,
                    'msg'=>'Error in Email or Password'
                ]);
            }
        }

    public function forgotpassword(Request $request){
        
        $user = DB::table('users')->where('email', $request->email)->first();
        if($user){
            $code = mt_rand(100000,999999);
            DB::table('users')->where('email',$request->email)->update(['forget_password' => $code]);
            $details = ['email' => $request->email,'code' => $code];
            Mail::to($request->email)->send(new \App\Mail\RecoverPassword($details)); 
            return response()->json([
                'status'=>200,
                'msg'=>'A message has been sent to your email '
            ]);

        }else{
            return response()->json([
                'status'=>500,
                'msg'=>'The email is not registered with us '
            ]);
        }
    }


    public function ConfirmationCode(Request $request){
          $check = DB::table('users')->where('email', $request->email)->where('forget_password', $request->code)->count();
          if($check > 0){
             return response()->json([
                  'status'=>200,
                  'msg'=>'good '
              ]);
          }else{
              return response()->json([
                  'status'=>500,
                  'msg'=>'Sorry, the code is incorrect'
              ]);
          }
      }
      
      
    public function ChangePassword(Request $request){
          $check = DB::table('users')->where('email', $request->email)->update(['password' =>  Hash::make($request->password)]);
          if($check){
             DB::table('users')->where('email',$request->email)->update(['forget_password' => '']);
             return response()->json([
                  'status'=>200,
                  'msg'=>'good '
              ]);
          }else{
              return response()->json([
                  'status'=>500,
                  'msg'=>'Sorry, the code is incorrect'
              ]);
          }
    }

    public function RegisterConfirmationCode(Request $request){
        $check = DB::table('users')->where('id', $request->id)->where('activation_code', $request->code)->count();
        if($check > 0){
            DB::table('users')->where('id', $request->id)->update(['status' => 1]);
            $email = DB::table('users')->where('id', $request->id)->value('email');
             Mail::to($email)->send(new \App\Mail\Welcom());
           return response()->json([
                'status'=>200,
                'msg'=>'good '
            ]);
        }else{
            return response()->json([
                'status'=>500,
                'msg'=>'Sorry, the code is incorrect'
            ]);
        }  
    }
}
