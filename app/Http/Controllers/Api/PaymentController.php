<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Card;


class PaymentController extends Controller
{

    public $currency           = 'AED';
    public $SHARequestPhrase   = '83/cPmhQ/ub0O3i8z0Qp4j-[';
    public $SHAResponsePhrase   = '64WSYHztai84ouu6X5/VpX-)';
    public $SHAType       = 'SHA-256';
    public $projectUrlPath     = '/paymobileapp'; 
    public $access_code     = '4hcMMrN56sS24IL0Mlxl'; 

    public function PayAgainCvv(){
                require_once base_path().'/paymobileapp/pay_agian.php';
    }
    
    public function PayAgain(){
        $token = Card::where('id',$_GET['method'])->value('token');
        $postData      = array(
            "command" => "PURCHASE",
            "access_code" => "4hcMMrN56sS24IL0Mlxl" ,
            "merchant_identifier"=> "LmIAIBAK",
            "merchant_reference" =>rand(0, getrandmax()) ,
            "amount"=> $this->convertFortAmount($_GET['amount'], $this->currency),
            "language" => "en",
            "token_name" => $token ,
            "currency"   => strtoupper($this->currency),
            "customer_email"  => "test@gmail.com",
            'return_url'  => $this->getUrl('route.php?r=processResponse'), 
            "card_security_code" =>  $_GET['cvv'] , 
        );
        
        $data = $this->calculateSignature($postData, 'request');
        $postData['signature'] = $data;
         $ch = curl_init();

        //set the url, number of POST vars, POST data
        $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0";
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
                //'Accept: application/json, application/*+json',
                //'Connection:keep-alive'
        ));
        // curl_setopt($ch, CURLOPT_URL, "https://sbcheckout.payfort.com/FortAPI/paymentApi");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_ENCODING, "compress, gzip");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects		
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); // The number of seconds to wait while trying to connect
        //curl_setopt($ch, CURLOPT_TIMEOUT, Yii::app()->params['apiCallTimeout']); // timeout in seconds
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

        $response = curl_exec($ch);
        
        //$response_data = array();
        //parse_str($response, $response_data);
        curl_close($ch);

        $array_result = json_decode($response, true);
        //dd($array_result);
        if (!$response || empty($array_result)) {
            return false;
        }
        if($array_result['response_message'] == "Technical problem dasddad"){
            return false;
        }
        
        if($array_result !== NULL){
              echo "<html><body onLoad=\"javascript: window.top.location.href='" . $array_result['3ds_url'] . "'\"></body></html>";
               exit;
        }else{
            return $array_result;
        }                      
    }
    
    public function convertFortAmount($amount, $currencyCode)
    {
        $new_amount = 0;
        $total = $amount;
        $decimalPoints    = $this->getCurrencyDecimalPoints($currencyCode);
        $new_amount = round($total, $decimalPoints) * (pow(10, $decimalPoints));
        return $new_amount;
    }

    public function getCurrencyDecimalPoints($currency)
    {
        $decimalPoint  = 2;
        $arrCurrencies = array(
            'JOD' => 3,
            'KWD' => 3,
            'OMR' => 3,
            'TND' => 3,
            'BHD' => 3,
            'LYD' => 3,
            'IQD' => 3,
        );
        if (isset($arrCurrencies[$currency])) {
            $decimalPoint = $arrCurrencies[$currency];
        }
        return $decimalPoint;
    }
    
    public function calculateSignature($arrData, $signType = 'request')
    {
        $shaString             = '';
        ksort($arrData);
        foreach ($arrData as $k => $v) {
            $shaString .= "$k=$v";
        }

        if ($signType == 'request') {
            $shaString = $this->SHARequestPhrase . $shaString . $this->SHARequestPhrase;
        }
        else {
            $shaString = $this->SHAResponsePhrase . $shaString . $this->SHAResponsePhrase;
        }
        $signature = hash($this->SHAType, $shaString);

        return $signature;
    }
    
        public function getUrl($path)
    {
        $scheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https://' : 'http://';
        $url = $scheme . $_SERVER['HTTP_HOST'] . $this->projectUrlPath .'/'. $path;
        return $url;
    }
    
    public function pay(){
        require_once base_path().'/paymobileapp/index.blade.php';
    }

    public function mobilePaymentSuccess(){
    }

    public function mobilePaymentFail(){
    }
    
    public function mobilePaymentDeclined(){
    }
    
}