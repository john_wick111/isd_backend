<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Validator;
use Auth;
use DB;
use Mail;
use App\Academy;
use App\Classes;
use App\OrderProduct;
use App\Product;
use App\Package;
use Carbon;
use PDF;
use App\Age;
use App\Vacation;
use App\Term;
use App\Subscription;
use App\Subscriber;
use App\Discount;
class AcademyController extends Controller
{

    public function AcademySingle($id){
        $data = Academy::where('academy', $id)->first(); 
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }
    
    public function Check(Request $request)
    {
        $data = Academy::orderBy('id', 'desc')->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function Classes(Request $request){
        
        // old without filter
        // $data = Classes::orderBy('id', 'desc')->get();
        // return response()->json([
        //     'status'=>200,
        //     'data'=>$data
        // ]);
        // new with filter -> Dev Mosib
        $data = Classes::orderBy('id', 'desc')->where('academy',$request->academy)->where('age_group',$request->age_category)->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function CheckAgeGroup(Request $request){
        $newAge = date('Y-m-d', strtotime($request->age));
        $age = (date('Y') - date('Y',strtotime(date('m/d/Y', strtotime($newAge)))));
        $group = Age::where('start','<=', $age)->where('end', '>=', $age)->value('id');
        $data = Classes::where('age_group', $group)->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function Packages(Request $request){
        $data = Package::where('sport', $request->sport)->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function CalculateAmount(Request $request){
        $package = Package::find($request->package)->toArray();
        $session_price = $package['price'];
        $firstTerm = array_values($package['term'])[0];
        $lastTerm = $package['term'][count($package['term'])-1];
        $package_start = strtotime($firstTerm['start_date']);
        $package_end = strtotime($lastTerm['end_date']);
        $numDays = $request->num_day;
        $startDate = date('Y-m-d', strtotime($request->startDate));
        $user_start_data = strtotime($startDate);
        $package_price = $package['day'.$request->num_day];
        $TrainingWeeks = (date( $package_end ) - date($package_start)) / 604800;
        $TrainingSession = $TrainingWeeks * $numDays;
        $vacations = Vacation::where('strart','>=', $startDate)->where('end', '<=', $lastTerm['end_date'])->get();
        $days = 0;
        $vacationsTotalDay = 0;
        for($i=0; $i< count($vacations); $i++){
            $datetime1 = strtotime($vacations[$i]['strart']);  
            $datetime2 = strtotime($vacations[$i]['end']); 
            $days = (int)(($datetime2 - $datetime1)/86400); 
            $vacationsTotalDay += $days;
        }
        $numBreakDay = $vacationsTotalDay / 7;
        $numBreakDay = number_format((float)$numBreakDay, 0, '.', '');

        $DayName =  date('D', strtotime($startDate));
        if($DayName == "Tue"){
            $daysRemaning  = ceil((date($package_end) - date($user_start_data)) / 86400 / 7);
            $sessionRemaing =  number_format((float)(($daysRemaning * $numDays) - 1) - ($numBreakDay * $numDays),0, '.', '');
        }else{
            $daysRemaning  = ceil((date($package_end) - date($user_start_data)) / 86400 / 7);            
            $sessionRemaing =  number_format((float)( $daysRemaning * $numDays) - ($numBreakDay * $numDays),0, '.', '');
        }

        $cost = $sessionRemaing * $session_price;
        $totalDiscount = $this->CalculateDiscount($request->user,$cost);
        $totalwithdiscount = $cost - $totalDiscount;
        return response()->json([
            'status'=>200,
            'sessionRemaing'=>$sessionRemaing,
            'session_price'=>number_format((float)$session_price, 0, '.', ''),
            'package_price'=>number_format((float)$package_price, 0, '.', ''),
            'cost'=> number_format((float)$cost, 2, '.', '')  ,
            'discount'=>$totalDiscount ,
            'totalwithdiscount'=> number_format((float)$totalwithdiscount, 2, '.', '') ,

        ]);
    }


    public function CalculateDiscount($id,$cost){
        $count = Subscription::where('user_id',$id)->where('status',1)->orderBy('subscription_fees', 'ASC')->count();
        if($count > 0){
            $data = Subscription::where('user_id',$id)->where('status',1)->orderBy('subscription_fees', 'ASC')->first('subscription_fees')->toArray();
            $MinCost = $data['subscription_fees'];
            $subtotal =  $MinCost < $cost ? $MinCost : $cost ;  
            $percent = Discount::where('number',$count)->value('discount');
            if($percent === null){
                $percent = 0;
            }
            return $totalDiscount =  ($percent / 100) * $subtotal;
        }else{
            return $totalDiscount =  0;

        }
    }

    public function MyPackages(Request $request){
        
        // dd($request->all());
        // exit;
        
        $firstTermID = array_values($request->terms)[0];
        $endTermID = array_reverse($request->terms)[0];
        
        // dd($firstTermID);
        
        $startDate = Term::where('id',$firstTermID)->value('start_date');
        $endtDate = Term::where('id',$endTermID)->value('end_date');
        $terms = trim(preg_replace('/\s*\([^)]*\)/', '', json_encode($request->terms)));
        
        // dd($terms);
        
        $myage = Age::where('academy', $request->academy)->where('start','<=', $request->years)->where('end', '>=', $request->years)->value('id');
        $data = Package::where('academy', $request->academy)->where('terms', $terms)->where('city', $request->city)->where('age_group', $myage)
        ->where(function ($query) {
            $query->where('day1_active', 1)
            ->orWhere('day2_active', 1)
            ->orWhere('day3_active', 1)
            ->orWhere('day4_active', 1)
            ->orWhere('day5_active', 1)
            ->orWhere('day6_active', 1)
            ->orWhere('day7_active', 1);
        })->get();
        
        // dd($data);

        $classes = Classes::where('academy', $request->academy)->where('age_group', $myage)->get();
        return response()->json([
            'status'=>200,
            'data'=>$data,
            'startDate' => $startDate,
            'lastDate' => $endtDate,
            'classes' => $classes
        ]);    
    }
    
    public function PackagesDetails(Request $request){
        $data = Package::find($request->id);
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);        
    }

    public function Vacations(){
        $data = Vacation::all();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function Terms(){
        $data = Term::all();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);
    }

    public function Subscriptions(){
        $data = User::where('relative',Auth::user()->id)->with('subscriptions','subscriptions.payments','subscriptions.comment')->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);        
    }

    public function SubscriberComments($id){
        $data = Comment::where('subscriber_id',$id)->get();
        return response()->json([
            'status'=>200,
            'data'=>$data
        ]);        
    }

    public function SendEmailcoach(Request $request){
        $to_email = User::find($request->id)->value('email');
        $from = 'test@isddubai.com'; 
        $data = array("body" => $request->content);

        Mail::send('emails.mail', $data, function($message) use ($to_email,$request,$from) {
            $message->to($to_email)->subject($request->subject);
            $message->from($from,$request->subject);
        });
        return response()->json([
            'status'=>200,
        ]); 
    }

    public function StoreSubscriptions(Request $request){

        // dd($request->all());

        $Subscription = new Subscription();
        $Subscription->academy = $request->academy;
        $Subscription->user_id = $request->user_id;
        $Subscription->start_date = $request->startDate;
        $Subscription->enf_date = $request->enf_date;
        $Subscription->subscription_fees  = $request->cost;
        $Subscription->age = $request->age;
        $Subscription->class = $request->class;
        $Subscription->num_days = $request->num_days;
        $Subscription->terms = $request->terms;
        $Subscription->package  = $request->package;
        $Subscription->type_num_days  = $request->type_num_days;
        $Subscription->save();

        foreach (json_decode($request->terms) as $key => $value) {
            DB::table('povite_subscriptions_terms')->insert(
                ['subscription_id' => $Subscription->id, 'term_id' => $value]
            );
        }

        $SubscriberCheck      = User::where('id',$request->name)->count();
        if($SubscriberCheck == 0){
            $phone = User::where('id',$request->user_id)->value('contacts');
            $user = new User();
            $user->relative = $request->user_id;
            $user->name = $request->name;
            $user->type = 2;
            $user->subscription = $Subscription->id;
            $user->academy = $request->academy;
            $user->email = $request->name.''.$request->user."2021@gmail.com";
            $user->password = Hash::make("123456");
            $user->age_group = $request->age;
            $user->date = $request->date_birth;
            $user->contacts = $phone;
            $user->save();
            
            $Subscription = Subscription::find($Subscription->id);
            $Subscription->subscriber = $user->id;
            $Subscription->save();
            
        }else{
            $Subscription = Subscription::find($Subscription->id);
            $Subscription->subscriber = $request->name;
            $Subscription->save();
        }
        
        
        // if($request->status == 1){
        //      $this->SendEmailClient($request->user_id,$Subscription->id);
        // }
        
        return response()->json([
            'status'=>200,
            'data' => $Subscription,
            'msg' => "Data Insert Success",
        ]);          
    }

    public function SendEmailClient($id,$Subscription){
        $email =  DB::table('users')->where('id',$id)->value('email');
        Mail::to($email)->send(new \App\Mail\NewSubscription($Subscription));  
    }

    public function SendEmailToAdmin(){
        $to_email = env('MAIL_FROM_ADDRESS');
        $from = env('MAIL_FROM_ADDRESS');
        $content = "test";
        $subject = "subject";
        $data = array("body" => $content);

        Mail::send('emails.mail', $data, function($message) use ($to_email,$subject,$content,$from) {
            $message->to($to_email)->subject($subject);
            $message->from($from,$subject);
        });
        return response()->json([
            'status'=>200,
        ]); 
    }

    public function SendEmailToAdminAttention($subscription,$academy){
        $to_email = "myisdalerts@isddubai.com";
        Mail::to($to_email)->send(new \App\Mail\AlertClassAttention($subscription,$academy));  
        return response()->json([
            'status'=>200,
        ]);
    }
    

    public function DownloadInvoice($id){
        $booking = Subscription::find($id);
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'user'  => $booking->client_id,
            'id'  => $id,
        ];
        $pdf = PDF::loadView('emails.invoice',compact('details'));
        return $pdf->download('Book.pdf');
    }

    public function SendEmailInvoice(){

    }
}