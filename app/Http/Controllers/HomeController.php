<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Sport;

use App\User;

use App\Product;

use Carbon\Carbon;

use App\Booking;

use App\Court;

use App\OrderProduct;

use Auth;

use Session;

use DB;

use Mail;

class HomeController extends Controller



{



    /**



     * Create a new controller instance.



     *



     * @return void



     */



    public function __construct()



    {



//        $this->middleware('auth');



    }







    /**



     * Show the application dashboard.



     *



     * @return \Illuminate\Contracts\Support\Renderable



     */



  

    public function bookings(){

       

       $sports=Sport::pluck('name','id');

       $durations=[1=>'60',2=>'90',3=>'120'];

       return view('pages.booking',['sports'=>$sports,'durations'=>$durations]);



    }



    public function checkSport(Request $request)

    { 

        $products=Product::where('stock','!=',0)->where('price','!=',0)->get(); 

        $id = $request->sport;

        if($request->id != 5){

        $time = $request->time;

        $duration = $request->duration;

        $to = \Carbon\Carbon::parse($request->time)->addMinutes($duration)->format('H:i');

        $day = date('l', strtotime($request->date));

        $data = Court::where('sports',$id)->whereHas('Times', function($query) use($day,$time,$to) {

            $query->where('day', '=', $day)->whereTime('from','<=',\Carbon\Carbon::parse($time))->whereTime('to','>=',\Carbon\Carbon::parse($to));

        })->get();

        }else{

            $data = Court::where('sports',$id)->get();

        }

      

       Session::put('to',$to); 

       Session::put('sport',$id); 

       Session::put('from',\Carbon\Carbon::parse($time)); 

       Session::put('date',$request->date); 



       return view('pages.court',['products'=>$products,'data'=>$data]);

    }



   

    public function addBooking(Request $request)

    {

        

        $to=Session::get('to');

        $from=Session::get('from');

        $sport=Session::get('sport');

        $date=Session::get('date');

        $client=User::find(Auth::user()->id);



        $size =  Court::where('id',$request->pitch)->value('size');

        $court =  Court::where('id',$request->pitch)->first();

       



        $namesport = DB::table('sports')->where('id',$sport)->value('name');

        $booking = new Booking();

        $booking->date = $date;

        $booking->from = $from;

        $booking->to = $to;

        $booking->pitch = $request->pitch;

        $booking->size = $size;

        $booking->price =$court->rate;

        $booking->dicsount = 0;

        $booking->total = $court->rate;

        $booking->client_id = Auth::user()->id;

        $booking->status = 0;

        $booking->penalty = 0;

        $booking->sports = $sport;

        $booking->code = rand ( 10000 , 99999 );

        $booking->save();

        

        if($request->products > 0){

            for($i= 0; $i < count($request->products); $i++){

                $product= Product::where('id',$request->products)->first();

                

                $OrderProduct = new OrderProduct();

                $OrderProduct->title = $product->title;

                $OrderProduct->price = $product->price;

                $OrderProduct->total = $product->price *  $request->qty[$i];

                $OrderProduct->count = $request->qty[$i];

                $OrderProduct->book_id  = $booking->id;

                $OrderProduct->save();

                $product = Product::where('id',$product->id)->first();

                $product->decrement('stock', $request->qty[$i]);

                $name=$OrderProduct->title.'*'.$OrderProduct->count;

                $total=$OrderProduct->total;

            }  



        }else{

        $name="---";

        $total="---";

        }   

       

        $details = [

            'Date' => $booking->date,

            'From' => $booking->from,

            'To' => $booking->to,

            'Pitch' => $booking->pitch,

            'Sports' => $namesport,

            'Products'=>$name,

            'Product Sum' => $total,

            'Total' => $booking->total,

            



        ];

        // $email = DB::table('users')->where('id',$client->id)->value('email');

        // Mail::to($email)->send(new \App\Mail\Booking($details));        

        

        return view('pages.review',['details'=>$details]);

    }




    public function emailVerification()

    {

       return view('pages.confirm');

    }



    private function validateBookings(Request $request) {



       



        $request->validate([



            'pitch'  => 'required',

        ]);



    }

   



    

}



