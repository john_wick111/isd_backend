<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class WaveoffAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Wave-off';
    }

    public function getIcon()
    {
        return 'voyager-dollar';
    }

    // public function getPolicy()
    // {
    //     return 'read';
    // }

    public function getAttributes()
    {
       if($this->data->{'status'} == 1)
        {
            return [
                'class' => 'btn btn-sm btn-primary',
            ];
        }
        else
        {
            return [
                'class' => 'btn btn-sm btn-primary hide',
            ];
        }
    }

    public function shouldActionDisplayOnDataType()
{
    return $this->dataType->slug == 'academy_payment';
}

    public function getDefaultRoute()
    {
        $data = $this->data->toArray();

        return route('waveoff.index', compact('data'));
    }
}